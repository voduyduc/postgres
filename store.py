import psycopg2

#connect to database
conn = psycopg2.connect(host="127.0.0.1", database = "hello", user = "DucVo", password = "MyMy2701")
cur = conn.cursor()

def insert(): #insert info to database (name, location(x,y) -> geography type)
        
    name = str(input("Input name: "))
    x = float(input("Input Longtitude: "))
    y = float(input("Input Latitude: "))

    sql = "insert into global_points (name, location)\
        values('%s',ST_GeogFromText('SRID=4326;POINT(%f %f)'));"%\
        (name,x,y)
    
    try:
        cur.execute(sql)
        conn.commit()

    except:
        print("Fail to insert")

def showDB(): #get data from database

    cur.execute("select id, ST_X(location::geometry) as longitude, ST_Y(location::geometry) as latitude from global_points;")
    rows = cur.fetchall()
    print("Show database:")
    for row in rows:
        print(row)

def distance(): #check if other locations near? and show them

    x = float(input("Input Longtitude: "))
    y = float(input("Input Latitude: ")) 
    r = float(input("Input radius in meter: "))

    sql = "SELECT name, ST_X(location::geometry) as longtitude, ST_Y(location::geometry) as latitude from global_points\
        WHERE ST_DWithin(location, ST_GeogFromText('SRID=4326;POINT(%f %f)'), %f);"%\
        (x,y,r)
    
    try:
        cur.execute(sql)
        rows = cur.fetchall()
        print("Locations near you: ")
        for row in rows:
            print(row)

    except:
        print("Fail to calculate")

while(input("Insert new location to Database: ") != ''):
    insert()
while(input("Insert your location: ") != ''):
    distance()

showDB()